﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var mon = new Dictionary<string, int>();

            mon.Add("January", 31);
            mon.Add("Febuary", 28);
            mon.Add("March", 31);
            mon.Add("April", 30);
            mon.Add("May", 31);
            mon.Add("June", 30);
            mon.Add("July", 31);
            mon.Add("August", 31);
            mon.Add("September", 30);
            mon.Add("October", 31);
            mon.Add("November", 30);
            mon.Add("December", 31);

            foreach (var x in mon)
            {
                if (x.Value == 31)
                {
                    Console.WriteLine($"{x.Key} has {x.Value} days");
                }
                else
                {

                }
            }
        }
    }
}

